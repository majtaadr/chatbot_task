import json

class Database():

    def __init__(self):
        with open("/Users/adrianamajtanova/Desktop/chatbot_task/actions/data.json") as j:
            self.db = json.loads(j.read())
    
    def get_all_animals(self):
       return list(self.db.keys())
       
    def get_attributes(self, animal, attribute):
        return self.db[animal][attribute]
        
