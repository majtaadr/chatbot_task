from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet
import random
from actions.database import *


class ActionChooseAnimal(Action):
    
    def name(self):
        return "action_choose_animal"
    
    def run(self, dispatcher, tracker, domain):
        db = Database()
        my_animal = random.choice(db.get_all_animals())
        dispatcher.utter_message(response="utter_animal_chosen")
        return [SlotSet("animal", my_animal)]

class ActionAnswerLegNumber(Action):

    string_to_num_converter = {'no' : 0, 'zero' : 0, 'two' : 2, 'four' : 4, 'six': 6}
    
    def convert_string_to_num(self,number):
        if number.isnumeric():
            return int(number)
        else:
            return self.string_to_num_converter[number]
          
    def name(self):
        return "action_answer_leg_number"
    
    def run(self, dispatcher, tracker, domain):
        db = Database()
        my_animal = tracker.get_slot("animal")
        intent = tracker.latest_message["intent"].get("name")
        
        legs = int(db.get_attributes(my_animal, "legs"))
        
        if intent == "ask_leg_number":
            dispatcher.utter_message(response="utter_leg_number", number=str(legs))
        else:
            entities = tracker.latest_message["entities"]
            if entities:
                leg_num_guess = self.convert_string_to_num(entities[0]['value'].lower())
                if leg_num_guess < legs:
                    dispatcher.utter_message(response="utter_wrong_guess_legs", comp="more", number=str(leg_num_guess))
                elif leg_num_guess > legs:
                    dispatcher.utter_message(response="utter_wrong_guess_legs", comp="less", number=str(leg_num_guess))
                else:
                    dispatcher.utter_message(response="utter_correct_guess_legs", number=str(leg_num_guess))
            else:
                dispatcher.utter_message(response="utter_uknown_input")
        return []
        
class ActionEvaluateGuess(Action):
    
    def name(self):
        return "action_evaluate_guess"
    
    def run(self, dispatcher, tracker, domain):
    
        my_animal = tracker.get_slot("animal")
        entities = tracker.latest_message["entities"]
        if entities:
            animal = entities[0]['value'].lower()
            if animal == my_animal:
                message = "Congratulations, you guessed the animal correctly, it is a " + my_animal + " indeed."
            else:
                message = "No, it is not a " + animal
            dispatcher.utter_message(text=message)
        else:
            dispatcher.utter_message(response="utter_uknown_input")
            
        return []
        
class ActionAnswerSkills(Action):
    
    def name(self):
        return "action_answer_skills"
    
    def run(self, dispatcher, tracker, domain):
        
        db = Database()
        my_animal = tracker.get_slot("animal")
        entities = tracker.latest_message["entities"]
        
        if entities:
            skill = entities[0]['value'].lower()
            if skill in db.get_attributes(my_animal, "skills"):
                message = "Yes, the animal can " + skill
            else:
                message = "No, the animal can't " + skill
            dispatcher.utter_message(text=message)
        else:
            dispatcher.utter_message(response="utter_uknown_input")
            
        return []

class ActionAnswerBodyCoverings(Action):
    
    def name(self):
        return "action_answer_body_coverings"
    
    def run(self, dispatcher, tracker, domain):
    
        db = Database()
        my_animal = tracker.get_slot("animal")
        entities = tracker.latest_message["entities"]
    
        if entities:
            covering = entities[0]['value'].lower()
            if covering == db.get_attributes(my_animal, "body_covering"):
                message = "Yes, it has " + covering
            else:
                message = "No, it doesn't have " + covering
            dispatcher.utter_message(text=message)
        else:
            dispatcher.utter_message(response="utter_uknown_input")
        return []

